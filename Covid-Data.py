import requests
import pandas as pd
from bs4 import BeautifulSoup

page = requests.get('https://www.mohfw.gov.in/')
soup = BeautifulSoup(page.content,'html.parser')
week = soup.find(id = 'cases')
items = week.find_all("th")
# print(items[0].get_text())
items = [i.get_text() for i in items]
#print(items)
rows = week.find_all('tr')
# print(rows[1].find_all('td'))
oo = [i.find_all('td') for i in rows]
# oo = oo.get_text()
#oo = [i for i in range(len(oo))]
pq = []
for i in range(1,len(oo)):
    rp = []
    for i in oo[i]:
        rp.append(i.get_text().strip('\n').strip('#'))
    # print(rp)
    pq.append(rp)
data = pd.DataFrame(pq, columns = ['S.no','Name of State / UT','Total Confirmed cases *','Cured/Discharged/Migrated','Death'])
print(data)
data.to_csv('File.csv',index = False)