import requests
from bs4 import BeautifulSoup

page = requests.get('https://forecast.weather.gov/MapClick.php?lat=34.09979000000004&lon=-118.32721499999997#.XoJrMogzY2w')
soup = BeautifulSoup(page.content,'html.parser')
week = soup.find(id = 'seven-day-forecast-body')
#print(week)
items = week.find_all(class_='tombstone-container')
#print(items[0])
print(items[1].find(class_='period-name').get_text())
print(items[1].find(class_='short-desc').get_text())
print(items[1].find(class_='temp').get_text())
