import requests
import pandas as pd
from bs4 import BeautifulSoup

page = requests.get('https://www.cuchd.in/faculty-list/')
soup = BeautifulSoup(page.content,'html.parser')

table = soup.find(class_ = 'table table-hover table-bordered results')

tk = table.find_all('tr')

FacultyList = []

for var in range(2,len(tk)):
    FacultyList.append([i.get_text().strip('\n') for i in tk[var].find_all('td')])
    
df = pd.DataFrame(FacultyList,columns = ['Name','Institute Name','Designation'])

# print(df)  
# Printing the Data Frame

df.to_csv('CuFaculty.csv',index = False) # Storing the DataFrame into .csv File